** Vimeo Staff Picks App (Sr Engineer)**

Build a single view mobile application (iOS or Android, your choice). If you choose to build for iOS, Swift or Objc are both fair game.

Obtain a "client credentials grant" OAuth token from the Vimeo API at app launch (or whatever juncture you feel is most appropriate).  

Use this token to fetch the first page of Staff Picks content from the Vimeo API. The response JSON will describe a list of “video” objects. 

Display the contents of this list using a list view (iOS: UICollectionView, UITableView; Android: ListView). Within each cell in the list, display the video’s thumbnail image and the video creator’s name.

Please do not use any third party libraries. The UX/UI is yours to design, completely up to you.

Refer to the API documentation on the Staff Picks endpoint:
https://developer.vimeo.com/api/endpoints/channels#/{channel_id}

As well as the API documentation on making Unauthenticated Requests:
https://developer.vimeo.com/api/authentication#unauthenticated-requests

The application should first and foremost meet the above requirements. Other priority considerations are clarity of architectural design and code readability. Anything above and beyond is extra credit.