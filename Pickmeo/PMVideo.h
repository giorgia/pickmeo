//
//  PMVideo.h
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/17/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMPicture.h"
#import "PMUser.h"

@interface PMVideo : NSObject

@property (nonatomic, strong) NSString  *name;
@property (nonatomic, strong) NSString  *desc;
@property (nonatomic, strong) NSString  *link;

@property (nonatomic, strong) PMPicture *pictures;
@property (nonatomic, strong) PMUser    *user;

+ (NSArray *)videosFromData:(NSArray *)data;

@end
