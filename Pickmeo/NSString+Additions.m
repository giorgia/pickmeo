//
//  NSString+Additions.m
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/15/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (NSString *)base64EncodedString
{
    NSData *plainData       = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String  = [plainData base64EncodedStringWithOptions:0];
    
    return base64String;
}

@end
