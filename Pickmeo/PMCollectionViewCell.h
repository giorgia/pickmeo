//
//  PMCollectionViewCell.h
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/17/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView  *thumbImageView;
@property (strong, nonatomic) IBOutlet UILabel      *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel      *videoNameLabel;

- (void)loadThumbnailFromUrl:(NSString *)url;

@end
