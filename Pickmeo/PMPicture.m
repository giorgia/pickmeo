//
//  PMPicture.m
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/17/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import "PMPicture.h"
#import "PMSize.h"

@implementation PMPicture

+ (PMPicture *)pictureFromData:(NSDictionary *)data
{
    PMPicture *picture  = [[PMPicture alloc] init];
    picture.uri         = data[@"uri"];
    picture.sizes       = [PMSize sizesFromData:data[@"sizes"]];
    
    return picture;
}

@end
