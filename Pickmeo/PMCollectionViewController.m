//
//  PMCollectionViewController.m
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/17/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import "PMCollectionViewController.h"
#import "PMCollectionViewCell.h"
#import "PMPicture.h"
#import "PMVideo.h"
#import "PMUser.h"
#import "PMSize.h"
#import "Vimeo.h"

#define MARGIN      2
#define CELL_HEIGHT 170

@interface PMCollectionViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) NSArray *videos;

@end

@implementation PMCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [[Vimeo sharedClient] authorizeComplention:^(NSError *error) {
        [[Vimeo sharedClient] staffPicksVideosComplention:^(NSArray *videos, NSError *error) {
            if (!error) {
                self.videos = videos;
                [self.collectionView reloadData];
            }
        }];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.videos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PMCollectionViewCell *cell = (PMCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];

    // Permit the autoresize of the image and the labels after rotation
    [[cell contentView] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];

    // Configure the cell
    PMVideo *video  = self.videos[indexPath.row];
    PMSize *size    = video.pictures.sizes[2];

    cell.userNameLabel.text     = video.user.name;
    cell.videoNameLabel.text    = video.name;
    [cell loadThumbnailFromUrl:size.link];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait
        || [[UIApplication sharedApplication] statusBarOrientation] ==UIInterfaceOrientationPortraitUpsideDown)
    {
        return CGSizeMake(self.view.frame.size.width  - MARGIN * 2, CELL_HEIGHT);
    }
    
    return CGSizeMake(self.view.frame.size.width / 3 - MARGIN * 2, CELL_HEIGHT);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.collectionView performBatchUpdates:nil completion:nil];
}


@end
