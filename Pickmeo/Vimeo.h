//
//  Vimeo.h
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/15/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMHTTPManager.h"

@interface Vimeo : PMHTTPManager

@property (nonatomic, strong) NSString *token;

+ (instancetype)sharedClient;
- (void)authorizeComplention:(void (^) (NSError *error))complention;
- (void)staffPicksVideosComplention:(void (^) (NSArray *videos, NSError *error))complention;

@end
