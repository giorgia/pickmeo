//
//  PMCollectionViewCell.m
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/17/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import "PMCollectionViewCell.h"
#import "Vimeo.h"

@implementation PMCollectionViewCell

- (void)loadThumbnailFromUrl:(NSString *)url
{
    [[[Vimeo sharedClient] downloadImageFromURL:url complention:^(UIImage *image, NSError *error)
      {
          if (!error) {
              self.thumbImageView.image = image;
          }
      }] resume];
}

@end
