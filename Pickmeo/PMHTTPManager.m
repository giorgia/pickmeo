//
//  PMHTTPManager.m
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/15/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import "PMHTTPManager.h"
#import "NSDictionary+Additions.h"

static NSString *errorDomain = @"com.gii.pickmeo";

@implementation PMHTTPManager

- (id)initWithBaseURL:(NSURL *)baseURL
{
    self = [super init];
    if (self) {
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        [sessionConfig setHTTPAdditionalHeaders:@{@"Accept": @"application/json"}];
        
        _baseURL    = baseURL;
        _imageCache = [[NSCache alloc] init];
        _session    = [NSURLSession sessionWithConfiguration:sessionConfig];
    }
    return self;
}

- (NSMutableURLRequest *)serializeRequestWithMethod:(NSString *)method
                                               path:(NSString *)path
                                         parameters:(NSDictionary *)parameters
                                            headers:(NSDictionary *)headers
{
    NSURL *url = [self.baseURL URLByAppendingPathComponent:path];
    
    NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    mutableRequest.HTTPMethod           = method;
    NSString *serializedParameters      = [parameters serializeParameters];

    if (parameters) {
        // This check should be more generic
        if ([method isEqualToString:@"POST"]) {
            [mutableRequest setHTTPBody:[serializedParameters dataUsingEncoding:NSUTF8StringEncoding]];
            
        } else {
            [mutableRequest setURL:[NSURL URLWithString:[url.absoluteString stringByAppendingFormat:@"?%@", serializedParameters]]];
        }
    }
    
    if (headers) {
        for (NSString *key in [headers keyEnumerator]) {
            [mutableRequest addValue:headers[key] forHTTPHeaderField:key];
        }
    }
    
    return mutableRequest;
}

- (NSURLSessionDataTask *)taskWithRequest:(NSURLRequest *)request
                              complention:(void (^) (id JSON, NSError *error))complention
{
    return [self.session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                    if (error) {
                        complention(nil, error);
                        return;
                    }
                    NSError *JSONError = nil;
                    id JSON = [NSDictionary JSONDeserialization:data error:&JSONError];
                    
                    if (httpResponse.statusCode >= 400 && JSON) {
                        NSError *error = [NSError errorWithDomain:errorDomain code:httpResponse.statusCode userInfo:JSON];
                        complention(nil, error);
                    } else {
                        complention(JSON, JSONError);
                    }
                });
            }];
}

- (NSURLSessionDataTask *)getPath:(NSString *)path
                       parameters:(NSDictionary *)parameters
                      complention:(void (^) (id JSON, NSError *error))complention
{
    NSURLRequest *request = [self serializeRequestWithMethod:@"GET" path:path parameters:parameters headers:nil];
    return [self taskWithRequest:request complention:complention];
}

- (NSURLSessionDataTask *)postPath:(NSString *)path
                        parameters:(NSDictionary *)parameters
                           headers:(NSDictionary *)headers
                       complention:(void (^) (id JSON, NSError *error))complention
{
    NSURLRequest *request = [self serializeRequestWithMethod:@"POST" path:path parameters:parameters headers:headers];
    return [self taskWithRequest:request complention:complention];
}


- (NSURLSessionDownloadTask *)downloadImageFromURL:(NSString *)stringURL
                                   complention:(void (^) (UIImage *image, NSError *error))complention
{
    UIImage *image = [self.imageCache objectForKey:stringURL];
    if (image) {
        complention(image, nil);
        return nil;
    }
    
    return [self.session downloadTaskWithURL:[NSURL URLWithString:stringURL] completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error)
            {
                if (error) {
                    complention(nil, error);
                    return;
                }
                
                NSData *data = [NSData dataWithContentsOfURL:location];
                UIImage *image = [UIImage imageWithData:data];
                [self.imageCache setObject:image forKey:stringURL];
                dispatch_async(dispatch_get_main_queue(), ^{
                    complention(image, error);
                });
    }];
}

@end
