//
//  PMSize.m
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/17/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import "PMSize.h"

@implementation PMSize

+ (PMSize *)sizeFromData:(NSDictionary *)data
{
    PMSize *size        = [[PMSize alloc] init];
    size.width          = data[@"width"];
    size.height         = data[@"height"];
    size.link           = data[@"link"];
    
    return size;
}

+ (NSArray *)sizesFromData:(NSArray *)data
{
    NSMutableArray *sizes = [NSMutableArray array];
    for (NSDictionary *sizeData in data) {
        PMSize *size = [PMSize sizeFromData:sizeData];
        [sizes addObject:size];
    }
    return sizes;
}

@end
