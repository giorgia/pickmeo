//
//  Vimeo.m
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/15/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import "Vimeo.h"
#import "PMVideo.h"
#import "NSString+Additions.h"

static NSString *vimeoBaseURL                   = @"https://api.vimeo.com";
static NSString *vimeoClientAuthorizePath       = @"/oauth/authorize/client";
static NSString *vimeoChannelsPath              = @"/channels/%@/videos";

static NSString *clientId                       = @"35b65b86968106e4467be77b0f373f65401e00dd";
static NSString *clientSecret                   = @"8eAJ3fRiznFezFxcKZP+R+71/wQinpg59pkjKX/tMjFc8PuzSjUfQWsgvNbxQNdHUY/SLun9k5tYGLmzIMiboLBxkJoZt8XrS5RBOCDKbXPVAZkO+KuBkPRsicqjFWCw";

@implementation Vimeo

+ (instancetype)sharedClient {
    static Vimeo *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[Vimeo alloc] initWithBaseURL:[NSURL URLWithString:vimeoBaseURL]];
    });
    
    return _sharedClient;
}

- (NSString *)authorizationValue
{
    NSString *credentials = [NSString stringWithFormat:@"%@:%@", clientId, clientSecret];
    NSString *authValue   = [NSString stringWithFormat:@"basic %@", [credentials base64EncodedString]];
    
    return authValue;
}


- (void)authorizeComplention:(void (^) (NSError *error))complention
{
    NSDictionary *params    = @{@"grant_type"       : @"client_credentials"};
    NSDictionary *headers   = @{@"Content-Type"     : @"application/x-www-form-urlencoded",
                                @"Authorization"    :[self authorizationValue]};
    
    [[self postPath:vimeoClientAuthorizePath
        parameters:params
           headers:headers
       complention:^(id JSON, NSError *error)
    {
        if (error) {
            complention(error);
        } else {
            if (JSON[@"access_token"]) {
                self.token = JSON[@"access_token"];
                complention(nil);
            }
        }
        
    }] resume];
}

- (void)staffPicksVideosComplention:(void (^) (NSArray *videos, NSError *error))complention
{
    NSDictionary *params = @{@"access_token" : self.token};
    
    NSString *staffPicksVideosPath = [NSString stringWithFormat:vimeoChannelsPath, @"staffpicks"];
    
    [[self getPath:staffPicksVideosPath parameters:params complention:^(id JSON, NSError *error) {
        if (error) {
            complention(nil, error);
        } else {
            NSArray *videos = [PMVideo videosFromData:JSON[@"data"]];
            complention(videos, error);
        }
    }] resume];
    
}

@end
