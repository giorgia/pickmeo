//
//  PMUser.h
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/17/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMPicture.h"

@interface PMUser : NSObject

@property (nonatomic, strong) NSString  *name;
@property (nonatomic, strong) PMPicture *pictures;

+ (PMUser *)userFromData:(NSDictionary *)data;

@end
