//
//  AppDelegate.h
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/15/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

