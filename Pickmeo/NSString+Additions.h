//
//  NSString+Additions.h
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/15/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (NSString *)base64EncodedString;

@end
