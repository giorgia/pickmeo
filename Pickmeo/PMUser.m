//
//  PMUser.m
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/17/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import "PMUser.h"
#import "PMPicture.h"

@implementation PMUser

+ (PMUser *)userFromData:(NSDictionary *)data
{
    PMUser  *user   = [[PMUser alloc] init];
    user.name       = data[@"name"];
    user.pictures   = [PMPicture pictureFromData:data[@"pictures"]];
    
    return user;
}

@end
