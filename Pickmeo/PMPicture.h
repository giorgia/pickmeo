//
//  PMPicture.h
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/17/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMPicture : NSObject

@property (nonatomic, strong) NSString *uri;
@property (nonatomic, strong) NSArray  *sizes;

+ (PMPicture *)pictureFromData:(NSDictionary *)data;

@end
