//
//  PMHTTPManager.h
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/15/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface PMHTTPManager : NSObject

@property (nonatomic, strong) NSCache               *imageCache;
@property (nonatomic, strong) NSURLSession          *session;
@property (nonatomic, strong) NSURL                 *baseURL;

- (id)initWithBaseURL:(NSURL *)baseURL;

- (NSMutableURLRequest *)serializeRequestWithMethod:(NSString *)method
                                               path:(NSString *)path
                                         parameters:(NSDictionary *)parameters
                                            headers:(NSDictionary *)headers;

- (NSURLSessionDataTask *) getPath:(NSString *)path
                        parameters:(NSDictionary *)parameters
                       complention:(void (^) (id JSON, NSError *error))complention;

- (NSURLSessionDataTask *) postPath:(NSString *)path
                         parameters:(NSDictionary *)parameters
                            headers:(NSDictionary *)headers
                        complention:(void (^) (id JSON, NSError *error))complention;

- (NSURLSessionDownloadTask *)downloadImageFromURL:(NSString *)stringURL
                                       complention:(void (^) (UIImage *image, NSError *error))complention;

@end
