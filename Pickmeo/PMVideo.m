//
//  PMVideo.m
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/17/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import "PMVideo.h"
#import "PMPicture.h"

@implementation PMVideo

+ (PMVideo *)videoFromData:(NSDictionary *)data
{
    PMVideo *video  = [[PMVideo alloc] init];
    video.name      = data[@"name"];
    video.desc      = data[@"description"];
    video.link      = data[@"link"];
    video.pictures  = [PMPicture pictureFromData:data[@"pictures"]];
    video.user      = [PMUser userFromData:data[@"user"]];
    
    return video;
}

+ (NSArray *)videosFromData:(NSArray *)data
{
    NSMutableArray *videos = [NSMutableArray array];
    for (NSDictionary *videoData in data) {
        PMVideo *video = [PMVideo videoFromData:videoData];
        [videos addObject:video];
    }
    return videos;
}

@end
