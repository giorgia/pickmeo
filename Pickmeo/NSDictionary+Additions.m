//
//  NSDictionary+Additions.m
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/15/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import "NSDictionary+Additions.h"

@implementation NSDictionary (Additions)

- (NSString *)serializeParameters
{
    NSMutableArray *pairs = [NSMutableArray array];
    for (NSString *key in [self keyEnumerator])
    {
        id value                    = self[key];
        NSString *URLEncodedString  = [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [pairs addObject:[NSString stringWithFormat:@"%@=%@", key, URLEncodedString]];
    }
    
    return [pairs componentsJoinedByString:@"&"];
}

+ (id)JSONDeserialization:(NSData *)data error:(NSError **)error
{
    id JSON = [NSJSONSerialization JSONObjectWithData:data
                                              options:NSJSONReadingAllowFragments
                                                error:error];

    return JSON;
}

@end
