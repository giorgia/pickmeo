//
//  PMSize.h
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/17/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMSize : NSObject

@property (nonatomic, strong) NSNumber *width;
@property (nonatomic, strong) NSNumber *height;
@property (nonatomic, strong) NSString *link;

+ (PMSize *)sizeFromData:(NSDictionary *)data;
+ (NSArray *)sizesFromData:(NSArray *)data;

@end
