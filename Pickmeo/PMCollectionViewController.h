//
//  PMCollectionViewController.h
//  Pickmeo
//
//  Created by Giorgia Marenda on 3/17/15.
//  Copyright (c) 2015 Giorgia Marenda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMCollectionViewController : UICollectionViewController

@end
